from django.shortcuts import render, redirect, get_object_or_404
from dogs.models import Dog
from dogs.forms import DogForm
import math

# Create your views here.
def dog_list(request):
    dogs = Dog.objects.all()
    context = {
        "dog_list": dogs,
    }
    return render(request, "dogs/list.html", context)

def show_dog(request, id):
    dog = get_object_or_404(Dog, id=id)
    # bcs_9 = "is about 40 percent overweight, and has a body condition score of 9/9"
    # bcs_8 = "is about 30 percent overweight, and has a body condition score of 8/9."
    # bcs_7 = "is about 20 percent overweight, and has a body condition score of 7/9."
    # bcs_6 = "is about 10 percent overweight, and has a body condition score of 6/9."
    # bcs_5 = "is at a good weight, and has a body condition score of around 5/9."
    # bcs_uhoh = "is too skinny, something may be going on, please see a vet."
    weight = Dog("weight_in_pounds")

    context = {
        #  "bcs_8": bcs_8,
        #  "bcs_7": bcs_7,
        #  "bcs_6": bcs_6,
        #  "bcs_5": bcs_5,
        #  "bcs_uhoh": bcs_uhoh,
        "dog_object": dog,
         "weight": weight,
    }

    return render(request, "dogs/detail.html", context)

def create_dog(request):
    if request.method == "POST":
        form = DogForm(request.POST)
        if form.is_valid():
            dog = form.save(False)
            dog.save()
            return redirect("dog_list")
    else:
        form = DogForm()
        context = {
            "form": form,
        }
        return render(request, "dogs/create.html", context)

def edit_dog(request, id):
    dog = Dog.objects.get(id=id)
    if request.method == "POST":
        form=DogForm(request.POST, instance=dog)
        if form.is_valid():
            form.save()
        return redirect("show_dog", id=id)
    else:
        form = DogForm(instance=dog)
        context = {
            "dog_object": dog,
            "dog_form": form,

        }
    return render(request, "dogs/edit.html", context)

def about(request):
    about = "This is an educational/entertaining site about dogs. This is for educational purposes only. Please consult a veterinarian if you have concerns about your pet."
    context = {
        "about": about,
    }
    return render(request, "dogs/about.html", context)
