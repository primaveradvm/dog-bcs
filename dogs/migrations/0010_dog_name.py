# Generated by Django 5.0.2 on 2024-02-08 22:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("dogs", "0009_alter_dog_title"),
    ]

    operations = [
        migrations.AddField(
            model_name="dog",
            name="name",
            field=models.CharField(max_length=50, null=True),
        ),
    ]
