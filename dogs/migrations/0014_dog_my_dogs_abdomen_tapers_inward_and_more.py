# Generated by Django 5.0.2 on 2024-02-09 02:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        (
            "dogs",
            "0013_rename_does_your_dogs_waist_taper_inward_dog_my_dogs_waist_tapers_inward",
        ),
    ]

    operations = [
        migrations.AddField(
            model_name="dog",
            name="my_dogs_abdomen_tapers_inward",
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name="dog",
            name="my_dogs_waist_tapers_inward",
            field=models.BooleanField(default=False),
        ),
    ]
