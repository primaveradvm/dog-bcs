# Generated by Django 5.0.2 on 2024-02-08 22:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("dogs", "0004_remove_colormodel_name_alter_colormodel_color"),
    ]

    operations = [
        migrations.CreateModel(
            name="Dog",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(max_length=200)),
                ("picture", models.URLField()),
                (
                    "rib_feel",
                    models.CharField(
                        choices=[
                            (
                                "5",
                                "super easy, no fat at all over them, my dogs ribs stick out",
                            ),
                            ("4", "I can feel ribs with some minimal fat covering"),
                            ("3", "I can feel ribs with slight fat covering"),
                            ("2", "hard to feel ribs, pushing through significant fat"),
                            ("1", "my dog has ribs?"),
                        ],
                        max_length=300,
                    ),
                ),
            ],
        ),
        migrations.DeleteModel(
            name="ColorModel",
        ),
    ]
