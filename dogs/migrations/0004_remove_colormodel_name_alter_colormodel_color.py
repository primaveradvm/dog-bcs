# Generated by Django 5.0.2 on 2024-02-08 00:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("dogs", "0003_alter_colormodel_color"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="colormodel",
            name="name",
        ),
        migrations.AlterField(
            model_name="colormodel",
            name="color",
            field=models.CharField(
                choices=[
                    ("1", "1"),
                    ("2", "2"),
                    ("3", "3"),
                    ("4", "4"),
                    ("5", "5"),
                    ("6", "6"),
                    ("7", "7"),
                    ("8", "8"),
                ],
                default="1",
                max_length=20,
            ),
        ),
    ]
