from django.db import models
from django.conf import settings


# Create your models here.
ribs_choices = (
    ('5','super easy, no fat at all over them, my dogs ribs stick out'),
    ('4','I can feel ribs with some minimal fat covering'),
    ('3','I can feel ribs with slight fat covering'),
    ('2','hard to feel ribs, pushing through significant fat'),
    ('1','my dog has ribs?'),
)

title_choices = (
    ('7','muddiness'),
    ('6','stinkiness'),
    ('5','majesty'),
    ('4','highness'),
    ('3','hairyness'),
    ('2','cuteness'),
    ('1','fatso'),
)

class Dog(models.Model):
  title = models.CharField(max_length=200, choices=title_choices, default='1')
  name = models.CharField(max_length=50, null=True)
  picture = models.URLField(null=True,blank=True)
  how_easily_can_I_feel_my_dogs_ribs = models.CharField(max_length=300, choices=ribs_choices, default='1')
  my_dogs_waist_tapers_inward = models.BooleanField(default=False)
  my_dogs_abdomen_tapers_inward = models.BooleanField(default=False)
  weight_in_pounds = models.IntegerField()



  author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="recipes",
        on_delete=models.CASCADE,
        null=True,
    )
  def __str__(self):
        return self.title
