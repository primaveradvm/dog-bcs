from django.urls import path
from dogs.views import dog_list, show_dog, create_dog, about, edit_dog

urlpatterns=[
    path("", dog_list, name="dog_list"),
    path("<int:id>/", show_dog, name="show_dog"),
    path("create/", create_dog, name="create_dog"),
    path("about/", about, name="about"),
    path("<int:id>/edit/", edit_dog, name="edit_dog"),
]
