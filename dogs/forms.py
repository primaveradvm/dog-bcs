from django.forms import ModelForm
from dogs.models import Dog

class DogForm(ModelForm):
    class Meta:
        model = Dog
        fields = [
            "title",
            "name",
            "picture",
            "how_easily_can_I_feel_my_dogs_ribs",
            "my_dogs_waist_tapers_inward",
            "my_dogs_abdomen_tapers_inward",
            "weight_in_pounds",
        ]
